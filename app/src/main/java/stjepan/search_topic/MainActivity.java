package stjepan.search_topic;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;

public class MainActivity extends Activity {

    @BindView(R.id.lvSearchTopic)
    ListView lvSearchTopic;
    @BindView(R.id.btAddQuery)
    Button btAddQuery;
    @BindView(R.id.etNewQuery)
    EditText etNewQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ArrayList<SearchTopic> searchTopicsList = loadSearchTopics();
        SearchTopicAdapter adapter = new SearchTopicAdapter(searchTopicsList);
        lvSearchTopic.setAdapter(adapter);
    }
    private ArrayList<SearchTopic> loadSearchTopics() {
        ArrayList<SearchTopic> searchTopicsList = new ArrayList<>();

        String[] searchTopicsArray = getResources().getStringArray(R.array.searchTopics);

        for (int i = 0; i < searchTopicsArray.length; i++){
            searchTopicsList.add(new SearchTopic(searchTopicsArray[i]));
        }
        return searchTopicsList;
    }
    @OnClick(R.id.btAddQuery)
    public void addQuery(View v){
        String newQuery = this.etNewQuery.getText().toString();
        SearchTopic newTopic = new SearchTopic(newQuery);
        ((SearchTopicAdapter) this.lvSearchTopic.getAdapter()).addSearchTopic(newTopic);
    }
    @OnItemClick(R.id.lvSearchTopic)
    public void onListItemClick(int position){
        SearchTopic searchTopic = (SearchTopic) this.lvSearchTopic.getAdapter().getItem(position);

        Intent implicitIntent = new Intent();
        implicitIntent.setAction(Intent.ACTION_SEARCH);
        implicitIntent.putExtra(SearchManager.QUERY, searchTopic.getQuery());

        if (canBeCalled(implicitIntent)){
            startActivity(implicitIntent);
        }
        else{
            Toast.makeText(this, "No activity can handle the request", Toast.LENGTH_SHORT).show();
        }
    }
    @OnItemLongClick(R.id.lvSearchTopic)
        public boolean onListItemLongClick(int position){
        ((SearchTopicAdapter) this.lvSearchTopic.getAdapter()).removeSearchTopic(position);
            return true;
    }
    private boolean canBeCalled(Intent implicitIntent) {
        PackageManager packageManager = this.getPackageManager();
        if(implicitIntent.resolveActivity(packageManager) != null){
            return true;
        }
        else{
            return false;
        }
    }
}
