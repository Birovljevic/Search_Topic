package stjepan.search_topic;

/**
 * Created by Stjepan on 9.10.2017..
 */

public class SearchTopic {
    private String mQuery;

    public SearchTopic(String query){
        this.mQuery = query;
    }
    public String getQuery(){
        return this.mQuery;
    }

}
