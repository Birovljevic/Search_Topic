package stjepan.search_topic;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Stjepan on 9.10.2017..
 */

public class SearchTopicAdapter extends BaseAdapter{

    private ArrayList<SearchTopic> mSearchTopic;

    public SearchTopicAdapter(List<SearchTopic> topicList){
        this.mSearchTopic = new ArrayList<>();
        this.mSearchTopic.addAll(topicList);
    }
    @Override
    public int getCount() {
        return this.mSearchTopic.size();
    }
    @Override
    public Object getItem(int position) {
        return this.mSearchTopic.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SearchTopicViewHolder holder;

        if(convertView == null){
            convertView = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.list_item_search_topic, parent, false);
            holder = new SearchTopicViewHolder(convertView);
            convertView.setTag(holder);
        }
        else{
            holder = (SearchTopicViewHolder) convertView.getTag();
        }

        SearchTopic searchTopic = this.mSearchTopic.get(position);
        holder.tvSearchTopic.setText(searchTopic.getQuery());

        return convertView;
    }
    static class SearchTopicViewHolder {

        @BindView(R.id.tvSearchTopic) TextView tvSearchTopic;

        public SearchTopicViewHolder(View view) {
            ButterKnife.bind(this, view);}
    }
    public void addSearchTopic(SearchTopic searchTopic){
        this.mSearchTopic.add(searchTopic);
        this.notifyDataSetChanged();
    }
    public void removeSearchTopic(int position){
        this.mSearchTopic.remove(position);
        this.notifyDataSetChanged();
    }
}
